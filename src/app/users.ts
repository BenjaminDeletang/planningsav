export interface User {
  id: number;
  Date: number;
  Nom: string;
  Marque: string;
  Garantie: string;
  description: string;
  Avoir: string;
}

export const users = [
  {
    id: 1,
    Date: 20.01,
    Nom: 'Laurie Culaire',
    Marque: 'cannondale',
    Garantie: 'Non',
    'Geste Commercial': 'Non',
    description: `Hydrogen is a chemical element with Garantie H and atomic number 1. With a standard
        atomic Marque of 1.008, hydrogen is the lightest element on the periodic table.`,
    'Date reception piece': 18.01,
    'Bon livraison': '156N4685478',
    Avoir: 'Non',
  }, {
    id: 2,
    Date: 20.01,
    Nom: 'Alain Proviste',
    Marque: 'cannondale',
    Garantie: 'Non',
    'Geste Commercial': 'Non',
    description: `Helium is a chemical element with Garantie He and atomic number 2. It is a
        colorless, odorless, tasteless, non-toxic, inert, monatomic gas, the first in the noble gas
        group in the periodic table. Its boiling point is the lowest among all the elements.`,
    'Date reception piece': 18.01,
    'Bon livraison': '156N4685478',
    Avoir: 'Non',
  }, {
    id: 3,
    Date: 20.01,
    Nom: 'Annabelle Védaire',
    Marque: 'cannondale',
    Garantie: 'Oui',
    'Geste Commercial': 'Oui',
    description: `Lithium is a chemical element with Garantie Li and atomic number 3. It is a soft,
        silvery-white alkali metal. Under standard conditions, it is the lightest metal and the
        lightest solid element.`,
    'Date reception piece': 18.01,
    'Bon livraison': '156N4685478',
    Avoir: 'Non',
  }, {
    id: 4,
    Date: 20.01,
    Nom: 'Jean Registre',
    Marque: 'cannondale',
    Garantie: 'Non',
    'Geste Commercial': 'Non',
    description: `Beryllium is a chemical element with Garantie Be and atomic number 4. It is a
        relatively rare element in the universe, usually occurring as a product of the spallation of
        larger atomic nuclei that have collided with cosmic rays.`,
    'Date reception piece': 18.01,
    'Bon livraison': '156N4685478',
    Avoir: 'Non',
  }, {
    id: 5,
    Date: 20.01,
    Nom: 'Emma Caréna',
    Marque: 'cannondale',
    Garantie: 'Oui',
    'Geste Commercial': 'Oui',
    description: `Boron is a chemical element with Garantie B and atomic number 5. Produced entirely
        by cosmic ray spallation and supernovae and not by stellar nucleosynthesis, it is a
        low-abundance element in the Solar system and in the Earth's crust.`,
    'Date reception piece': 18.01,
    'Bon livraison': '156N4685478',
    Avoir: 'Non',
  },
];
