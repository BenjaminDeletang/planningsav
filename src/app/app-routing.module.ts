import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {HomeComponent} from './home/home.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {ModifyComponent} from './modify/modify.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    pathMatch: 'full'
  },
  {
    path: 'modify',
    component: ModifyComponent,
    // children: [
    //   { path: 'modify/:id', component: ModifyComponent }
    //   ]
  },
  {
    path: 'modify/:id',
    component: ModifyComponent
  },
  {
    path: '404',
    component: NotFoundComponent
  },
  // otherwise redirect to home
  {
    path: '**',
    redirectTo: ''
  }


];

@NgModule({
  imports: [RouterModule.forRoot(routes), RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
