import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {HttpClient, HttpHeaders, HttpErrorResponse} from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import {environment} from '../../environments/environment';
import {AppLocalStorageService} from './app-local-storage.service';

const apiUrl = 'http://localhost/planningsavBO/public';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {

  constructor(private http: HttpClient, private appLocalStorage: AppLocalStorageService) { }

  private getHeaders() {
    let token = '';
    token = this.appLocalStorage.getItem('ACCESS_TOKEN');
    const httpOptions = {
      headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: `Bearer ${token}`
        }
      )
    };
    return httpOptions;
  }

  private handleError(error: HttpErrorResponse) {
    try {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    } catch (e) {
    }

    return throwError('Something bad happened; please try again later.');
  }

  private extractData(res: Response) {
    const body = res;
    return body || { };
  }

  get(endpoint: string): Observable<any> {
    return this.http.get(environment.AUTH_SERVER_ADDRESS + '/' + endpoint, this.getHeaders()).pipe(
      map(this.extractData),
      catchError(this.handleError)
    );
  }

  getById(endpoint: string, id: string): Observable<any> {
    const url = `${environment.AUTH_SERVER_ADDRESS}/${endpoint}/${id}`;
    return this.http.get(url, this.getHeaders()).pipe(
      map(this.extractData),
      catchError(this.handleError));
  }

  post(endpoint: string, data): Observable<any> {
    const url = `${environment.AUTH_SERVER_ADDRESS}/${endpoint}`;
    return this.http.post(url, data, this.getHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  update(endpoint, id: string, data): Observable<any> {
    const url = `${environment.AUTH_SERVER_ADDRESS}/${endpoint}/${id}`;
    return this.http.put(url, data, this.getHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }

  delete(endpoint, id: string): Observable<{}> {
    const url = `${environment.AUTH_SERVER_ADDRESS}/${endpoint}/${id}`;
    return this.http.delete(url, this.getHeaders())
      .pipe(
        catchError(this.handleError)
      );
  }
}
