import { Component, OnInit } from '@angular/core';
import {HomeComponent} from '../home/home.component';
import {ActivatedRoute} from '@angular/router';
import {User, users} from '../users';

@Component({
  selector: 'app-modify',
  templateUrl: './modify.component.html',
  styleUrls: ['./modify.component.scss']
})
export class ModifyComponent implements OnInit {

  user: User|undefined;

  constructor(
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    const routeParams = this.route.snapshot.paramMap;
    const userIdFromRoute = Number(routeParams.get('id'));

    // Trouver l'utilisateur qui correspond à l'identifiant fourni en route.
    this.user = users.find(user => user.id === userIdFromRoute);
  }

}
